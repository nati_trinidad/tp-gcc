from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User

# Create your tests here.


class test_usuario(TestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(
            username='admingcc', email='ramirosilvero95@gmail.com', password='admingcc')

    def test_login(self):
        self.assertEqual(self.client.login(
            username='admingcc', password='admingcc'), True)

    def test_logout(self):
        self.client.login(username='admingcc', password='admingcc')
        response = self.client.get("/logout/")
        self.assertEqual(response.status_code, 302)

    def test_inicio(self):
        self.client.login(username='admingcc', password='admingcc')
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
