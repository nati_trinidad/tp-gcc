# TP-GCC

<h3>Proyecto django creado para el tp de GCC</h3>
<h3>montado en heroku</h3>
<h4>FP-UNA 2019</h4>
<h4>Ramiro Silvero</h4>



<strong>Desarrollo en:</strong> https://gcc-ramiro-dev.herokuapp.com

Tareas ejecutadas:
<ul>
    <div><li>Checking: compila la aplicación con python manage.py check y verifica que no haya errores</li></div>
    <div><li>Testing: corre las pruebas unitarias de la aplicación verificando si todas son aprobadas</li></div>
    <div><li>Dev_deploy: instala las librerias necesarias para correr un script que despliega la aplicación en el entorno de heroku de desarrollo</li></div>
</ul>
Usuarios:
<div><ul>
    <div><li>ramiro</li></div>
    <div><li>contraseña: ramirogcc </li></div><div> </div>
    <div><li>augusto</li></div>
    <div><li>contraseña: adminadmin </li></div><div> </div>
    <div><li>camila</li></div>
    <div><li>contraseña: adminadmin </li></div><div> </div>
    <div><li>oscar</li></div>
    <div><li>contraseña: adminadmin </li></div><div> </div>
</ul></div>

<strong>Producción en: </strong> https://gcc-ramiro-prod.herokuapp.com
<div><ul>
    <div><li>Checking: compila la aplicación con python manage.py check y verifica que no haya errores</li></div>
    <div><li>Testing: corre las pruebas unitarias de la aplicación verificando si todas son aprobadas</li></div>
    <div><li>Prod_deploy: instala las librerias necesarias para correr un script que despliega la aplicación en el entorno de heroku de produccion</li></div>
</ul></div>
Usuarios:
<ul>
    <div><li>admin</li></div>
    <div><li>contraseña: gcctp2019 </li><div> </div>
</ul>
<div>
    Antes de cada fase en cualquiera de los 2 entornos, se ejecuta un script llamado before_script, que muestra la versión de python e instala 
    las dependencias de la aplicacion que estan anotadas en requirements.txt con pip install -r requirements.txt
</div>
<div>
    <strong>Las aplicaciones de heroku fueron manipuladas para guardar los usuarios en sus respectivas bases de datos</strong>
</div>